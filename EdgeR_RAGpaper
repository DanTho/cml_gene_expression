#dthomson
#July2019
#Rscripts for RAG paper
#adapted mostly from edgeR manual with a number of follow up plots

####outputs
#lcpm table
#tables of differentially expressed genes (DxvsLBC, DxvsMBC, LBCvsDx )
#MDS plot highlighting RAG genes
#MD plot
#interactive MDS plot
#interactive MD plot
#heatmap of V(D)J recombination pathway labeled with Dx, LBC and MBC
#heatmap of RAG transcriptional regulators
#heatmap of most differentially expressed genes (with different version depending on what list of most differentially expressed genes was used (LBCvsDx, LBCvsMBC,MBCvsDx))
#boxplots of selected genes at Dx, LBC and MBC
#beeswarm plots of selected genes at Dx, LBC and MBC

##################################

#install packages

if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
BiocManager::install("edgeR")
install.packages("gtools")
BiocManager::install("Homo.sapiens")

#previous method of installing packages is out of date
#source("http://bioconductor.org/biocLite.R") #doesn't work anymore
#biocLite() #doesn't work anymore
#BiocManager::install()

install.packages("edgeR")
install.packages("gplots")
install.packages("heatmap3")
BiocManager::install("Glimma")
install.packages("vioplots")
install.packages("beeswarm")

library(gplots)
library(vioplot)
library(Glimma)
library(heatmap3)
library(limma)
library(edgeR)
library(stringr)
library(gtools)
library(beeswarm)

###

#set working directory
setwd("~/Documents/R_geneexpression_2019/")
getwd()

###
#upload genecounts.tsv files from STAR

#for 70 patient samples cohort with removed 4_LBC and 14_LBC
dirName <- "/data/molpath/data/analysis/170511SueBranfordCombined_Nextseq/170517DanThomsonAnalysis/copied_andreas/GeneCounts_rm4_LBC_14_LBC/"

#for 72 patient samples cohort with removed 4_LBC and 14_LBC but including 1_MBC 2_MBC 
#dirName <- "/data/molpath/data/analysis/170511SueBranfordCombined_Nextseq/170517DanThomsonAnalysis/copied_andreas/GeneCounts_add1/"

#for 72 patient sample cohort with outliers
#dirName <- "/data/sacgf/molpath/data/analysis/180501SusanBranfordCombinedAnalysis//BC_vs_Dx_analysis/GeneCounts"

###
#Define files;
dirName
files <- mixedsort(list.files(path = dirName, pattern="*\\.tsv$"))

# Files need to be suiably named (using the correct delimiters  '.' and '_')so that files/ sample names and groups can be defined as per the following; eg 1_MBC.csv
# Get the data; skip the first few rows that contain unmapped, multimapping, noFeature and ambiguous entries
# Column 3 contains the sense datadf
x <- readDGE(files, path=dirName,columns=c(1,3),skip=4)
class(x)
dim(x)
x$samples
head(x$counts,n=5)
ncol(x)

# Define sample names
sampleNames<- sapply(strsplit(files, split='.', fixed=TRUE), function(x) (x[1]))
sampleNames
sampleNames.short<- sapply(strsplit(files, split='_', fixed=TRUE), function(x) (x[1]))
sampleNames.short
colnames(x) <- sampleNames

# Store number of samples
sampleNumber <- length(sampleNames)

############################
# Get gene information
library(Homo.sapiens)
columns(Homo.sapiens)
keytypes(Homo.sapiens)
geneid <- rownames(x)
genes <- select(Homo.sapiens, keys=geneid, columns=c("ENTREZID","GENENAME", "TXCHROM", "DEFINITION","GO","PATH"), keytype="SYMBOL")
head(genes)
dim(genes)

#Identify duplicated gene symbols
dup <- genes$SYMBOL[duplicated(genes$SYMBOL)]
genes[genes$SYMBOL %in% dup,][1:5,]

#Keep only the first entry for each gene symbol
mat <- match(geneid, genes$SYMBOL)
genes <- genes[mat,]
genes[genes$SYMBOL %in% dup,][1:5,]
dim(genes)
length(geneid)

#Add gene information to x
x$genes <- genes

# Define sample groups
group <- as.factor(sapply(strsplit(sampleNames,"[0-9]_"),function(x) (x[2])))
x$samples$group <- group
x$samples
cpdxgroup=which(group %in% "CP_Dx")
lbcgroup=which(group %in% "LBC")
#outlier=which(group %in% "LBCOutlier")   #not in all datasets
#lbcgroup=setdiff(lbcgroup,outlier)     #not in all datasets
mbcgroup=which(group %in% "MBC")
bcgroup=c(lbcgroup,mbcgroup)
lbc.dxgroup=c(lbcgroup,cpdxgroup)
sampleNames[lbcgroup]
sampleNames[mbcgroup]
sampleNames[cpdxgroup]

# Counts per million reads for the raw data
rawCPM <- cpm(x)
rawLCPM <- cpm(x, log=TRUE)

# About 700 genes are not expressed at all, in any sample
table(rowSums(x$counts==0)==sampleNumber)
keep.exprs <- rowSums(rawCPM[,lbcgroup]>0.5) > 0.8*length(lbcgroup) | rowSums(rawCPM[,mbcgroup]>0.5) > 0.8*length(mbcgroup) | rowSums(rawCPM[,cpdxgroup]>0.5) > 0.8*length(cpdxgroup)
x <- x[keep.exprs,, keep.lib.sizes=FALSE]
dim(x)

# Counts per million reads for the filtered data
lcpm <- cpm(x, log=TRUE)
ncol(lcpm[, c(lbcgroup,mbcgroup,cpdxgroup)])

#write lcpm file for excel
write.csv(lcpm, file = "~/Documents/R_geneexpression_2019/lcpm.wihtoutMBC1.2.csv", row.names = TRUE, col.names = TRUE)

############################
# Produce a MDS plot for the normalized data. 
col.group <- group
levels(col.group) <-gsub("MBC","blue",levels(col.group))
levels(col.group) <-gsub("LBC","red",levels(col.group))
levels(col.group) <-gsub("CP_Dx","green",levels(col.group))
#levels(col.group) <-gsub("LBCOutlier","red",levels(col.group))      # not in all datasets
col.group <- as.character(col.group)

#plot
dev.off()
par(mfrow=c(1,1), pty="m" )
plotMDS(x, labels=sampleNames, top = 250, col=col.group, main="MDS - Groups (Normalized)",xlab = "Dim 1", ylab = "Dim 2")

#save plot as EPS
dev.off()

##################################################################################################
#heatmap of VDJ recombination pathway

VDJ_pathway <- data.frame(lcpm[c("RAG1","RAG2","DNTT","LIG4","NHEJ1","TCF3","HMGB1","PRKDC","DCLRE1C"), ])

# Normalize tidy data, transpose for row wise normalisation
dat.n <- scale(t(VDJ_pathway))
# Put data back in original form
dat.tn <- t(dat.n)
# Check means are zero and std devs are 1
round(colMeans(dat.n),1)
# Calculate distance between experiments in rows
d1 <- dist(dat.n,method = "euclidean", diag = FALSE, upper = FALSE)
# Show distances
round(d1,3)
# Calculate distance between proteins in rows
d2 <- dist(dat.tn,method = "euclidean", diag = FALSE, upper = TRUE)
# Clustering distance between experiments using Ward linkage
c1 <- hclust(d1, method = "mcquitty", members = NULL)
# Clustering distance between proteins using Ward linkage
c2 <- hclust(d2, method = "mcquitty", members = NULL)

#plot
dev.off()
my_palette <- colorRampPalette(c("red","black","green"))(n = 25) # Set colours for heatmap, 25 increments
par(mar = c(.5,.5,.5,4) +0.1)
heatmap3(dat.tn,                     # Tidy, normalised data
         Colv=as.dendrogram(c1),     # Experiments clusters in cols
         Rowv=as.dendrogram(c2),     # Protein clusters in rows
         density.info="histogram",   # Plot histogram of data and colour key
         trace="none",               # Turn of trace lines from heat map
         col = my_palette,           # Use my colour scheme
         cexRow=2.5,cexCol=0.4,     # Amend row and column label fonts
         ColSideColors = col.group, #take away rowlabels
         labCol = as.vector(sampleNames.short),
         margins = c(4,12),
         keysize = 0.8
)
legend("topright",legend=c("Lymphoid Blast Crisis","Myeloid Blast Crisis","Diagnosis/Chronic Phase"), fill=c("red","blue","green", bty = "n", cex = 0.5))

#save plot as EPS for illustrator
dev.off()

#################
#heatmat of inhibitors and inducers of RAG expression
col.group.lbc.dx<- group[c(lbc.dxgroup)]
levels(col.group.lbc.dx)<-gsub("LBC", "red", levels(col.group.lbc.dx))
levels(col.group.lbc.dx)<-gsub("CP_Dx", "green", levels(col.group.lbc.dx))
col.group.lbc.dx<-as.character(col.group.lbc.dx)

VDJ_regulators <- data.frame(lcpm[c("IL7","AKT1","SP1","STAT5B","PAX5", "MYB","LEF1","FOXO1","FOXP1","BLNK","IKZF3","BCL11A","NFATC1","RUNX1","TCF3","IKZF1"), lbc.dxgroup])
ncol(VDJ_regulators)
nrow(VDJ_regulators)

# Normalize tidy data, transpose for row wise normalisation
dat.n <- scale(t(VDJ_regulators))
# Put data back in original form
dat.tn <- t(dat.n)
# Check means are zero and std devs are 1
round(colMeans(dat.n),1)
# Calculate distance between experiments in rows
d1 <- dist(dat.n,method = "euclidean", diag = FALSE, upper = FALSE)
# Show distances
round(d1,3)
# Calculate distance between proteins in rows
d2 <- dist(dat.tn,method = "euclidean", diag = FALSE, upper = TRUE)
#is.na(d2) <- 0
# Clustering distance between experiments using Ward linkage
c1 <- hclust(d1, method = "ward.D2", members = NULL)
# Clustering distance between proteins using Ward linkage
c2 <- hclust(d2, method = "mcquitty", members = NULL)

#plot
par(mfrow=c(2,1),cex=0.5) # Make 2 rows, 1 col plot frame and shrink labels
plot(c1); plot(c2) # Plot both cluster dendrograms
my_palette <- colorRampPalette(c("red","black","green"))(n = 25) # Set colours for heatmap, 25 increments

rowsidecolours <- c("gray","gray","gray","gray","black","black","black","black","black","black","black","black","black","black","black","black")
length(rowsidecolours)

dev.off()
par(cex.main=1) # Shrink title fonts on plot
heatmap3(dat.tn,                     # Tidy, normalised data
         Colv=as.dendrogram(c1),     # Experiments clusters in cols
         Rowv=as.dendrogram(c2),     # Protein clusters in rows
         density.info="histogram",   # Plot histogram of data and colour key
         trace="none",               # Turn of trace lines from heat map
         col = my_palette,           # Use my colour scheme
         cexRow=1,cexCol=0.75,     # Amend row and column label fonts
         ColSideColors = c(col.group.lbc.dx),
         labCol = sampleNames.short[lbc.dxgroup],  
         RowSideColors = rowsidecolours
)

legend("topright",legend=c("LBC","Dx"), fill=c("red","green", border = FALSE, bty = "n", y.intersp = 1, x =1, y = 1, cex = 0.3))

#save plot as eps for illustrator 
dev.off()

#################################################################################################
#Differential expression anlaysis

#define the design by groups
group
design <- model.matrix(~group)
colnames(design)
y <- estimateDisp(x, design)

#need to claculate dispersion values
fit<- glmFit(y,design)

#can choose glmQLFit or glmLRT method to create table of differential expression between groups
lrt.allvsall<-glmLRT(fit,coef=2:3)
topTags(lrt.allvsall)
summary(decideTests(lrt.allvsall))
write.csv(lrt.allvsall$table, file="lrt.allvsall.csv")

lrt.LBCvsDx<-glmLRT(fit,contrast=c(-1,1,0))
topTags(lrt.LBCvsDx)
summary(decideTests(lrt.LBCvsDx))
write.csv(lrt.LBCvsDx$table, file="lrt.LBCvsDx.csv")

lrt.LBCvsMBC<-glmLRT(fit,contrast=c(-1,0,1))
topTags(lrt.MBCvsDx)
summary(decideTests(lrt.MBCvsDx))
write.csv(lrt.LBCvsMBC$table, file="lrt.LBCvsMBC.csv")

lrt.MBCvsLBC<-glmLRT(fit,contrast=c(0,-1,1))
topTags(lrt.MBCvsLBC)
summary(decideTests(lrt.MBCvsLBC))

lrt.LBCvsMBC<-glmLRT(fit,contrast=c(0,1,-1))
topTags(lrt.LBCvsMBC)
summary(decideTests(lrt.LBCvsMBC))
write.csv(lrt.LBCvsMBC$table, file="lrt.LBCvsMBC.csv")

#plot MD plots
dev.off()
par(cex=0.4, mar = c(10,10,10,10))
plotMD(lrt.LBCvsDx, main ="LBC vs diagnosis", cex.axis=3, cex.lab=3, cex.main=3)
#add annotations
text(lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2"),"logCPM"], lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2"),"logFC"], labels=c("DNTT","RAG1","RAG2"), adj=c(-0.1,-0.1), cex=3)
points(lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2"),"logCPM"], lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2"),"logFC"], col="black", cex=3)

dev.off()
par(mfrow=c(2,1),cex=0.4)
#abline(h=c(-1,1),col="blue")
plotMD(lrt.LBCvsDx, main ="LBC vs diagnosis", cex.axis=3, cex.lab=3, cex.main=3)
#add annotations
abline(h=c(-1,1),col="blue")    #not really necessary
text(lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logCPM"], lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logFC"], labels=c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"), adj=c(-0.1,-0.3), cex=3)
points(lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logCPM"], lrt.LBCvsDx$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logFC"], col="black", cex=3)

plotMD(lrt.LBCvsMBC, main ="LBC vs MBC", cex.axis=3, cex.lab=3, cex.main=3)
#add annotations
abline(h=c(-1,1),col="blue")  #not really necessary
text(lrt.LBCvsMBC$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logCPM"], lrt.LBCvsMBC$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logFC"], labels=c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"), adj=c(-0.1,-0.3), cex=3)
points(lrt.LBCvsMBC$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logCPM"], lrt.LBCvsMBC$table[c("DNTT","RAG1","RAG2","LIG4","NHEJ1","HMGB1","PRKDC","DCLRE1C"),"logFC"], col="black", cex=3)

#write differential expression tables for excel
write.csv(lrt.allvsall$table, file = "lrt.allvsall.csv")
write.csv(lrt.LBCvsDx$table, file = "lrt.LBCvsDx.csv")
write.csv(lrt.MBCvsDx$table, file = "lrt.MBCvsDx.csv")
write.csv(lrt.LBCvsMBC$table, file = "lrt.MBCvsLBC.csv")

#####################################################
#heatmap (1 of 4) of gene significantly differentially expressed between all groups

summary(decideTests(lrt.allvsall))
o.allvsall <- order(lrt.allvsall$table$PValue)
sig.allvsall<-o.allvsall[1:200]
lcpm.sig.allvsall <- data.frame(lcpm[c(sig.allvsall), ])

# Normalize tidy data, transpose for row wise normalisation
dat.n <- scale(t(lcpm.sig.allvsall))
# Put data back in original form
dat.tn <- t(dat.n)
# Check means are zero and std devs are 1
round(colMeans(dat.n),1)
# Calculate distance between experiments in rows
d1 <- dist(dat.n,method = "euclidean", diag = FALSE, upper = FALSE)
# Show distances
round(d1,3)
# Calculate distance between proteins in rows
d2 <- dist(dat.tn,method = "euclidean", diag = FALSE, upper = TRUE)
# Clustering distance between experiments using Ward linkage
c1 <- hclust(d1, method = "ward.D2", members = NULL)
# Clustering distance between proteins using Ward linkage
c2 <- hclust(d2, method = "ward.D2", members = NULL)

#plot
dev.off()
my_palette <- colorRampPalette(c("red","black","green"))(n = 25) # Set colours for heatmap, 25 increments
par(mar = c(.5,.5,.5,4) +0.1)
heatmap3(dat.tn,                     # Tidy, normalised data
         Colv=as.dendrogram(c1),     # Experiments clusters in cols
         Rowv=as.dendrogram(c2),     # Protein clusters in rows
         density.info="histogram",   # Plot histogram of data and colour key
         trace="none",               # Turn of trace lines from heat map
         col = my_palette,           # Use my colour scheme
         cexRow=0.2,cexCol=0.6,     # Amend row and column label fonts
         ColSideColors = col.group, 
         labRow = ,
         labCol = as.vector(sampleNames.short),
         margins = c(4,12),
         keysize = 0.8
)
legend("topright",legend=c("Lymphoid Blast Crisis","Myeloid Blast Crisis","Diagnosis/Chronic Phase"), fill=c("red","blue","green", bty = "n", cex = 0.5))
title("allvsall")

#save plot as EPS for illustrator
dev.off()

#################################################################
#heatmap (2 of 4) of gene significantly differentially expressed between LBC and Dx

summary(decideTests(lrt.LBCvsDx))
o.LBCvsDx <- order(lrt.LBCvsDx$table$PValue)
sig.LBCvsDx<-o.LBCvsDx[1:200]
lcpm.sig.LBCvsDx <- data.frame(lcpm[c(sig.LBCvsDx), ])

# Normalize tidy data, transpose for row wise normalisation
dat.n <- scale(t(lcpm.sig.LBCvsDx))
# Put data back in original form
dat.tn <- t(dat.n)
# Check means are zero and std devs are 1
round(colMeans(dat.n),1)
# Calculate distance between experiments in rows
d1 <- dist(dat.n,method = "euclidean", diag = FALSE, upper = FALSE)
# Show distances
round(d1,3)
# Calculate distance between proteins in rows
d2 <- dist(dat.tn,method = "euclidean", diag = FALSE, upper = TRUE)
# Clustering distance between experiments using Ward linkage
c1 <- hclust(d1, method = "ward.D2", members = NULL)
# Clustering distance between proteins using Ward linkage
c2 <- hclust(d2, method = "ward.D2", members = NULL)

#plot
dev.off()
my_palette <- colorRampPalette(c("red","black","green"))(n = 25) # Set colours for heatmap, 25 increments
par(mar = c(.5,.5,.5,4) +0.1)
heatmap3(dat.tn,                     # Tidy, normalised data
         Colv=as.dendrogram(c1),     # Experiments clusters in cols
         Rowv=as.dendrogram(c2),     # Protein clusters in rows
         density.info="histogram",   # Plot histogram of data and colour key
         trace="none",               # Turn of trace lines from heat map
         col = my_palette,           # Use my colour scheme
         cexRow=0.2,cexCol=0.4,     # Amend row and column label fonts
         ColSideColors = col.group, 
         labRow = ,
         labCol = as.vector(sampleNames.short),
         margins = c(4,12),
         keysize = 0.8
)
legend("topright",legend=c("Lymphoid Blast Crisis","Myeloid Blast Crisis","Diagnosis/Chronic Phase"), fill=c("red","blue","green", bty = "n", cex = 0.5))
title("LBCvsDx")

#save plot as EPS for illustrator
dev.off()

#################################################################
#heatmap (3 of 4) of gene significantly differentially expressed between MBC and Dx

summary(decideTests(lrt.MBCvsDx))
o.MBCvsDx <- order(lrt.MBCvsDx$table$PValue)
sig.MBCvsDx<-o.MBCvsDx[1:500]
lcpm.sig.MBCvsDx <- data.frame(lcpm[c(sig.MBCvsDx), ])

# Normalize tidy data, transpose for row wise normalisation
dat.n <- scale(t(lcpm.sig.MBCvsDx))
# Put data back in original form
dat.tn <- t(dat.n)
# Check means are zero and std devs are 1
round(colMeans(dat.n),1)
# Calculate distance between experiments in rows
d1 <- dist(dat.n,method = "euclidean", diag = FALSE, upper = FALSE)
# Show distances
round(d1,3)
# Calculate distance between proteins in rows
d2 <- dist(dat.tn,method = "euclidean", diag = FALSE, upper = TRUE)
# Clustering distance between experiments using Ward linkage
c1 <- hclust(d1, method = "ward.D2", members = NULL)
# Clustering distance between proteins using Ward linkage
c2 <- hclust(d2, method = "ward.D2", members = NULL)

#plot
dev.off()
my_palette <- colorRampPalette(c("red","black","green"))(n = 25) # Set colours for heatmap, 25 increments
par(mar = c(.5,.5,.5,4) +0.1)
heatmap3(dat.tn,                     # Tidy, normalised data
         Colv=as.dendrogram(c1),     # Experiments clusters in cols
         Rowv=as.dendrogram(c2),     # Protein clusters in rows
         density.info="histogram",   # Plot histogram of data and colour key
         trace="none",               # Turn of trace lines from heat map
         col = my_palette,           # Use my colour scheme
         cexRow=0.2,cexCol=0.4,     # Amend row and column label fonts
         ColSideColors = col.group, 
         labRow = ,
         labCol = as.vector(sampleNames.short),
         margins = c(4,12),
         keysize = 0.8
)
legend("topright",legend=c("Lymphoid Blast Crisis","Myeloid Blast Crisis","Diagnosis/Chronic Phase"), fill=c("red","blue","green", bty = "n", cex = 0.5))
title("MBCvsDx")

#save plot as EPS for illustrator
dev.off()

#################################################################
#heatmap (4 of 4) of gene significantly differentially expressed between LBC and MBC

summary(decideTests(lrt.MBCvsLBC))
o.MBCvsLBC <- order(lrt.MBCvsLBC$table$PValue)
sig.MBCvsLBC<-o.MBCvsLBC[1:200]
lcpm.sig.MBCvsLBC <- data.frame(lcpm[c(sig.MBCvsLBC), ])

# Normalize tidy data, transpose for row wise normalisation
dat.n <- scale(t(lcpm.sig.MBCvsLBC))
# Put data back in original form
dat.tn <- t(dat.n)
# Check means are zero and std devs are 1
round(colMeans(dat.n),1)
# Calculate distance between experiments in rows
d1 <- dist(dat.n,method = "euclidean", diag = FALSE, upper = FALSE)
# Show distances
round(d1,3)
# Calculate distance between proteins in rows
d2 <- dist(dat.tn,method = "euclidean", diag = FALSE, upper = TRUE)
# Clustering distance between experiments using Ward linkage
c1 <- hclust(d1, method = "ward.D2", members = NULL)
# Clustering distance between proteins using Ward linkage
c2 <- hclust(d2, method = "ward.D2", members = NULL)

#plot
dev.off()
my_palette <- colorRampPalette(c("red","black","green"))(n = 25) # Set colours for heatmap, 25 increments
par(mar = c(.5,.5,.5,4) +0.1)
heatmap3(dat.tn,                     # Tidy, normalised data
         Colv=as.dendrogram(c1),     # Experiments clusters in cols
         Rowv=as.dendrogram(c2),     # Protein clusters in rows
         density.info="histogram",   # Plot histogram of data and colour key
         trace="none",               # Turn of trace lines from heat map
         col = my_palette,           # Use my colour scheme
         cexRow=0.2,cexCol=0.4,     # Amend row and column label fonts
         ColSideColors = col.group, 
         labRow = ,
         labCol = as.vector(sampleNames.short),
         margins = c(4,12),
         keysize = 0.8
)
legend("topright",legend=c("Lymphoid Blast Crisis","Myeloid Blast Crisis","Diagnosis/Chronic Phase"), fill=c("red","blue","green", bty = "n", cex = 0.5))
title("MBCvsLBC")

#save plot as EPS for illustrator
dev.off()

#################################################################
#interactive MD and MDS plots
#glimma

rnaseq <- y
rnaseq$samples$group
rnaseq <- rnaseq[rowSums(cpm(rnaseq)>1)>=3,]
rnaseq <- calcNormFactors(rnaseq)
groups <- rnaseq$samples$group
glMDSPlot(rnaseq, groups=groups)

design <- model.matrix(~0+groups)
contrasts <- cbind(MBCvsDx=c(-1,0,1))
vm <- voomWithQualityWeights(rnaseq, design=design)
fit <- lmFit(vm, design=design)
fit <- contrasts.fit(fit, contrasts)
fit <- eBayes(fit)
dt <- decideTests(fit)
summary(dt)
glMDPlot(fit, status=dt, counts=vm, groups=groups, side.main="Symbols")

contrasts <- cbind(LBCvsMBC=c(0,1,-1), LBCvsDx=c(-1,1,0), MBCvsDx=c(-1,0,1))
design <- model.matrix(~0+groups)
contrasts <- cbind(LBCvsDx=c(-1,1,0))
vm <- voomWithQualityWeights(rnaseq, design=design)
fit <- lmFit(vm, design=design)
fit <- contrasts.fit(fit, contrasts)
fit <- eBayes(fit)
dt <- decideTests(fit)
summary(dt)
glMDPlot(fit, status=dt, counts=vm, groups=groups, side.main="Symbols")

design <- model.matrix(~0+groups)
contrasts <- cbind(LBCvsMBC=c(0,1,-1))
vm <- voomWithQualityWeights(rnaseq, design=design)
fit <- lmFit(vm, design=design)
fit <- contrasts.fit(fit, contrasts)
fit <- eBayes(fit)
dt <- decideTests(fit)summary(dt)
glMDPlot(fit, status=dt, counts=vm, groups=groups, side.main="Symbols")

#MDS plot
logcpm <- cpm(rnaseq, log=TRUE, normalized.lib.sizes=TRUE)
glMDSPlot(logcpm, groups=groups)

#####################################
#boxplots of RAG/ V(D)J recombination pathway
#for paper

#setwd("~/Documents/R_edgeRanalysis_Dx_BC/")

#install.packages("gplots")
library(gplots)
library(vioplot)

#load("/data/molpath/data/analysis/180501SusanBranfordCombinedAnalysis/BC_vs_Dx_analysis/.RData")
#load("/data/molpath/data/analysis/180501SusanBranfordCombinedAnalysis/BC_vs_Dx_analysis/.Rhistory")


#lcpm_normalised <- read.csv("/data/molpath/data/analysis/180501SusanBranfordCombinedAnalysis/BC_vs_Dx_analysis/lcpm_normalized.tsv", header = TRUE, sep = "\t")
#lcpm <- read.csv("/data/molpath/data/analysis/180501SusanBranfordCombinedAnalysis/BC_vs_Dx_analysis/lcpm_normalized.tsv", header = TRUE, sep = "\t")

dev.off()
#boxplots of RAG1,RAG2, DNTT expression from groups LBC MBC Dx

par(mfrow=c(2,4))
boxplot(lcpm["RAG1", c(cpdxgroup)], lcpm["RAG1", c(mbcgroup)], lcpm["RAG1", c(lbcgroup)], names = c("Dx","MBC", "LBC"),col =c("green","blue","red"), main="RAG1", cex.main =3, cex.axis=1.8, cex.lab=1.5, las=2,  ylab = "lcpm")
points(c(lcpm["RAG1", "17_CP_Dx"],lcpm["RAG1", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["RAG1", c(cpdxgroup)], lcpm["RAG1", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["RAG2", c(cpdxgroup)], lcpm["RAG2", c(mbcgroup)], lcpm["RAG2", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main ="RAG2", cex.main =3, cex.axis=1.8, cex.lab=1.5, las=2,ylab="lcpm")
#points(c(lcpm["RAG2", "17_CP_Dx"],lcpm["RAG2", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["RAG2", c(cpdxgroup)], lcpm["RAG2", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["DCLRE1C", c(cpdxgroup)], lcpm["DCLRE1C", c(mbcgroup)], lcpm["DCLRE1C", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "DCLRE1C",cex.main =3, cex.axis=1.8, cex.lab=1.5,las=2, ylab="lcpm")
#points(c(lcpm["DCLRE1C", "17_CP_Dx"],lcpm["DCLRE1C", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["DCLRE1C", c(cpdxgroup)], lcpm["DCLRE1C", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["PRKDC", c(cpdxgroup)], lcpm["PRKDC", c(mbcgroup)], lcpm["PRKDC", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "PRKDC",cex.main =3, cex.axis=1.8, cex.lab=1.5,las=2,ylab="lcpm")
#points(c(lcpm["PRKDC", "17_CP_Dx"],lcpm["PRKDC", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["PRKDC", c(cpdxgroup)], lcpm["PRKDC", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["DNTT", c(cpdxgroup)], lcpm["DNTT", c(mbcgroup)], lcpm["DNTT", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "DNTT",cex.main =3, cex.axis=1.8, cex.lab=1.5,las=2,ylab="lcpm")
#points(c(lcpm["DNTT", "17_CP_Dx"],lcpm["DNTT", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["DNTT", c(cpdxgroup)], lcpm["DNTT", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["NHEJ1", c(cpdxgroup)], lcpm["NHEJ1", c(mbcgroup)], lcpm["NHEJ1", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "NHEJ1",cex.main =3, cex.axis=1.8, cex.lab=1.5,las=2,ylab="lcpm")
#points(c(lcpm["NHEJ1", "17_CP_Dx"],lcpm["NHEJ1", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["NHEJ1", c(cpdxgroup)], lcpm["NHEJ1", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

#boxplot(lcpm["TCF3", c(cpdxgroup)], lcpm["TCF3", c(mbcgroup)], lcpm["TCF3", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "TCF3",cex.main =3, cex.axis=1.5,las=2,ylab="lcpm")

boxplot(lcpm["HMGB1", c(cpdxgroup)], lcpm["HMGB1", c(mbcgroup)], lcpm["HMGB1", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "HMGB1",cex.main =3, cex.axis=1.8, cex.lab=1.5,las=2,ylab="lcpm")
#points(c(lcpm["HMGB1", "17_CP_Dx"],lcpm["HMGB1", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["HMGB1", c(cpdxgroup)], lcpm["HMGB1", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["LIG4", c(cpdxgroup)], lcpm["LIG4", c(mbcgroup)], lcpm["LIG4", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "LIG4",cex.main =3, cex.axis=1.8, cex.lab=1.5,las=2,ylab="lcpm")
#points(c(lcpm["LIG4", "17_CP_Dx"],lcpm["LIG4", "17_MBC"]), col=c("red", "red"), pch = 24)
t.test(lcpm["LIG4", c(cpdxgroup)], lcpm["LIG4", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

#saved as eps
dev.off()

########################################
#combined boxplots and beeswarm plots
par(mfrow=c(3,3))

boxplot(lcpm["RAG1", c(cpdxgroup)], main="RAG1", ylab = "log counts per million", xlab ="Dx/CP", cex.axis=1, cex.main=3, cex.lab =1.5,  ylim=c(-2,9))
beeswarm(lcpm["RAG1", c(cpdxgroup)], col=c(cpdxgroupcolours,cpdxgroupcolours), pwcol = cpdxgroupcolours, pch=16, cex =1.5, main = "RAG1", ylab ="log count per million", add =TRUE)
#legend("topleft", c("major molecular response","myeloid blast crisis","lymphoid blast crisis"), title="patient endpoint", fill =c("green","blue","red"), cex=1.5)
boxplot(lcpm["RAG1", c(mbcgroup)],main="RAG1", xlab="MBC", cex.axis=1, cex.main=3, cex.lab =1.5,xlab="MBC", ylim=c(-2,9))
beeswarm(lcpm["RAG1", c(mbcgroup)], col="blue", pch=16, cex =1.5, main = "RAG1", ylab ="log count per million",add =TRUE)
boxplot(lcpm["RAG1", c(lbcgroup)],main="RAG1", xlab="LBC", ylim=c(-2,9), cex.axis=1, cex.main=3, cex.lab =1.5)
beeswarm(lcpm["RAG1", c(lbcgroup)], col="red", pch=16, cex =1.5, main = "RAG1", ylab ="log count per million", add =TRUE)

boxplot(lcpm["RAG2", c(cpdxgroup)], main="RAG2", ylab = "log counts per million", xlab ="Dx/CP", cex.axis=1, cex.main=3, cex.lab =1.5,  ylim=c(-7,7))
beeswarm(lcpm["RAG2", c(cpdxgroup)], col=c(cpdxgroupcolours,cpdxgroupcolours), pwcol = cpdxgroupcolours, pch=16, cex =1.5, main = "RAG1", ylab ="log count per million", add =TRUE)
#legend("topleft", c("Dx","MBC","LBC"), title="patients", fill =c("green","blue","red"), cex=2)
boxplot(lcpm["RAG2", c(mbcgroup)],main="RAG2", xlab="MBC", cex.axis=1, cex.main=3, cex.lab =1.5, ylim=c(-7,7))
beeswarm(lcpm["RAG2", c(mbcgroup)], col="blue", pch=16, cex =1.5, main = "RAG2", ylab ="log count per million", add =TRUE)
boxplot(lcpm["RAG2", c(lbcgroup)],main="RAG2", xlab="LBC", ylim=c(-7,7), cex.axis=1, cex.main=3, cex.lab =1.5)
beeswarm(lcpm["RAG2", c(lbcgroup)], col="red", pch=16, cex =1.5, main = "RAG2", ylab ="log count per million", add =TRUE)

boxplot(lcpm["DNTT", c(cpdxgroup)], main="DNTT", ylab = "log counts per million", xlab ="Dx/CP", cex.axis=1, cex.main=3, cex.lab =1.5,  ylim=c(-7,11))
beeswarm(lcpm["DNTT", c(cpdxgroup)], col=c(cpdxgroupcolours,cpdxgroupcolours), pwcol = cpdxgroupcolours, pch=16, cex =1.5, main = "RAG1", ylab ="log count per million", add =TRUE)
#legend("topleft", c("Dx","MBC","LBC"), title="patients", fill =c("green","blue","red"), cex=2)
boxplot(lcpm["DNTT", c(mbcgroup)],main="DNTT", xlab="MBC", cex.axis=1, cex.main=3, cex.lab =1.5, ylim=c(-7,11))
beeswarm(lcpm["DNTT", c(mbcgroup)], col="blue", pch=16, cex =1.5, main = "DNTT", ylab ="log count per million", add =TRUE)
boxplot(lcpm["DNTT", c(lbcgroup)],main="DNTT", xlab="LBC", ylim=c(-7,11), cex.axis=1, cex.main=3, cex.lab =1.5)
beeswarm(lcpm["DNTT", c(lbcgroup)], col="red", pch=16, cex =1.5, main = "DNTT", ylab ="log count per million", add =TRUE)

#saved as eps
dev.off()

#############################################3
#boxplots
#transcriptional regulators of RAG and V(D)J recombination (genes from Ochodnicka-Mackovicova et al 2016, journal of immunology)

par(mfrow=c(2,5))
boxplot(lcpm["IL7", c(cpdxgroup)], lcpm["IL7", c(mbcgroup)], lcpm["IL7", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "IL7", ylab="lcpm", las=2, cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["IL7", c(cpdxgroup)], lcpm["IL7", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"**",cex=2)
par(xpd=FALSE)

boxplot(lcpm["AKT1", c(cpdxgroup)], lcpm["AKT1", c(mbcgroup)], lcpm["AKT1", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "AKT1", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["AKT1", c(cpdxgroup)], lcpm["AKT1", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"***",cex=2)
par(xpd=FALSE)

boxplot(lcpm["STAT5B", c(cpdxgroup)], lcpm["STAT5B", c(mbcgroup)], lcpm["STAT5B", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "STAT5B", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["STAT5B", c(cpdxgroup)], lcpm["STAT5B", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"***",cex=2)
par(xpd=FALSE)

boxplot(lcpm["FOXO3", c(cpdxgroup)], lcpm["FOXO3", c(mbcgroup)], lcpm["FOXO3", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "FOXO3A", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["FOXO3", c(cpdxgroup)], lcpm["FOXO3", c(lbcgroup)])
t.test(lcpm["FOXO3", c(mbcgroup)], lcpm["FOXO3", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["SP1", c(cpdxgroup)], lcpm["SP1", c(mbcgroup)], lcpm["SP1", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "SP1", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["SP1", c(cpdxgroup)], lcpm["SP1", c(lbcgroup)])
#t.test(lcpm["SP1", c(mbcgroup)], lcpm["SP1", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"*",cex=2)
par(xpd=FALSE)

#RAG inducers
boxplot(lcpm["PAX5", c(cpdxgroup)], lcpm["PAX5", c(mbcgroup)], lcpm["PAX5", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "PAX5", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["PAX5", c(cpdxgroup)], lcpm["PAX5", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["LEF1", c(cpdxgroup)], lcpm["LEF1", c(mbcgroup)], lcpm["LEF1", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "LEF1", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["LEF1", c(cpdxgroup)], lcpm["LEF1", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["FOXO1", c(cpdxgroup)], lcpm["FOXO1", c(mbcgroup)], lcpm["FOXO1", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "FOXO1", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["FOXO1", c(cpdxgroup)], lcpm["FOXO1", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["BLNK", c(cpdxgroup)], lcpm["BLNK", c(mbcgroup)], lcpm["BLNK", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "BLNK", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["BLNK", c(cpdxgroup)], lcpm["BLNK", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

boxplot(lcpm["TCF3", c(cpdxgroup)], lcpm["TCF3", c(mbcgroup)], lcpm["TCF3", c(lbcgroup)], names = c("Dx", "MBC", "LBC"),col=c("green","blue","red"), main = "TCF3", ylab="lcpm", las=2,cex.main=3, cex.lab=1.8, cex.axis=1.8)
t.test(lcpm["TCF3", c(cpdxgroup)], lcpm["TCF3", c(lbcgroup)])
par(xpd=TRUE)
yrange<-par("usr")[3:4]
ypos<-yrange[2]+diff(yrange)/100
text(3,ypos+diff(yrange)/50,"****",cex=2)
par(xpd=FALSE)

#saved as eps
dev.off()

###################################################################################################